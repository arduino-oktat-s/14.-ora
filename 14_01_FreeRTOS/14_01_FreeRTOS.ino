//Szerző: Robotika Pécs
//Program: FreeRTOS bemutatása
//lib: https://downloads.arduino.cc/libraries/github.com/feilipu/FreeRTOS-10.4.3-8.zip

#include <Arduino_FreeRTOS.h>


void setup() {
  Serial.begin(9600);
  pinMode(13, OUTPUT);

  xTaskCreate(
    task1
    ,  "task1"   //task neve
    ,  128  // verem mérete
    ,  NULL
    ,  2  // prioritás
    ,  NULL );

  xTaskCreate(
    task2
    ,  "task2"
    ,  128
    ,  NULL
    ,  1
    ,  NULL );
}

void loop() {}

void task1() {
  while (1) {
    digitalWrite(13, HIGH);
    vTaskDelay(1000 / portTICK_PERIOD_MS); // várjon egy másodpercig
    digitalWrite(13, LOW);
    vTaskDelay(1000 / portTICK_PERIOD_MS); // várjon egy másodpercig
  }
}

void task2() {
  while (1) {
    Serial.println("Task2 folyamatos munkat vegez, mikozben task1 minden masodpercen megall a delaytol.");
    vTaskDelay(1);
  }
}
